import java.util.Scanner;

class Fahrkartenautomat
{
    static Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args)
    {
       while(true) {
           double zuZahlenderBetrag = fahrkartenbestellungErfassen();

           // Geldeinwurf
           // -----------
           double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

           // Fahrscheinausgabe
           // -----------------
           fahrkartenAusgeben();

           // Rückgeldberechnung und -Ausgabe
           // -------------------------------
           rueckgeldAusgeben(rückgabebetrag);
       }
    }
    public static double fahrkartenbestellungErfassen()
    {
        String[] fahrkartenBezeichnung = {
                "Einzelfahrschein Berlin AB",
                "Einzelfahrschein Berlin BC",
                "Einzelfahrschein Berlin ABC",
                "Kurzstrecke",
                "Tageskarte Berlin AB",
                "Tageskarte Berlin BC",
                "Tageskarte Berlin ABC",
                "Kleingruppen-Tageskarte Berlin AB",
                "Kleingruppen-Tageskarte Berlin BC",
                "Kleingruppen-Tageskarte Berlin ABC"
        };
        double[] fahrkartenPreis = {
                2.90,
                3.30,
                3.60,
                1.90,
                8.60,
                9.00,
                9.60,
                23.50,
                24.30,
                24.90
        };
        int fahrkartenId;
        int anzahlFahrkarten;
        double gesamtBetrag = 0.0;

        do {
            System.out.printf("%-16s%-40s%-16s%n", "Auswahlnummer", "Bezeichnung", "Preis in Euro");
            for (int i = 0; i < fahrkartenPreis.length; i++) {
                System.out.printf("%-16d%-40s%-4.2f€%n", (i + 1), fahrkartenBezeichnung[i], fahrkartenPreis[i]);
            }System.out.printf("%n%-16d%-40s%n", (fahrkartenPreis.length + 1), "Zum Bezahlen");
            do {
                System.out.print("Ihre Wahl: ");
                fahrkartenId = tastatur.nextInt();
            }
            while(fahrkartenId < 1 || fahrkartenId > fahrkartenPreis.length + 1);
            if(fahrkartenId != fahrkartenPreis.length + 1) {
                do {
                    System.out.print("Anzahl der Fahrkarten (1-10): ");
                    anzahlFahrkarten = tastatur.nextInt();
                }
                while (anzahlFahrkarten < 1 || anzahlFahrkarten > 10);
                gesamtBetrag += Math.round(fahrkartenPreis[fahrkartenId-1] * anzahlFahrkarten * 100.0) / 100.0;
            }
        }
        while(fahrkartenId != fahrkartenPreis.length + 1);
        return gesamtBetrag;
    }
    public static double fahrkartenBezahlen(double zuZahlen)
    {
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMünze;

        while(eingezahlterGesamtbetrag < zuZahlen)
        {
            System.out.println("Noch zu zahlen: " + String.format("%.2f", zuZahlen - eingezahlterGesamtbetrag) + " Euro");
            System.out.print("Eingabe (mind. 5Ct, höchstens 2,00 Euro): ");
            eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
            eingezahlterGesamtbetrag = Math.round(eingezahlterGesamtbetrag * 100.0) / 100.0;
        }
        return Math.round((eingezahlterGesamtbetrag - zuZahlen) * 100.0) / 100.0;
    }
    public static void fahrkartenAusgeben()
    {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            warte(250);
        }
        System.out.println("\n\n");
    }
    public static void rueckgeldAusgeben(double rückgabebetrag)
    {
        if(rückgabebetrag > 0.0)
        {
            System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%.2f",rückgabebetrag) + " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                muenzeAusgeben(2, "EURO");
                rückgabebetrag -= 2.0;
                rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                muenzeAusgeben(1, "EURO");
                rückgabebetrag -= 1.0;
                rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                muenzeAusgeben(50, "CENT");
                rückgabebetrag -= 0.5;
                rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                muenzeAusgeben(20, "CENT");
                rückgabebetrag -= 0.2;
                rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                muenzeAusgeben(10, "CENT");
                rückgabebetrag -= 0.1;
                rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                muenzeAusgeben(5, "CENT");
                rückgabebetrag -= 0.05;
                rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
            }
        }
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    }
    public static void warte(int millisekunden)
    {
        try {
            Thread.sleep(millisekunden);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public static void muenzeAusgeben(int betrag, String einheit)
    {
        System.out.println(betrag + " " + einheit);
    }
}